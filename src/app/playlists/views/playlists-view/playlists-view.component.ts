import { Component, OnInit } from "@angular/core";
import { NgIf } from "@angular/common";
import { Playlist } from "src/app/models/Playlist";
import { ActivatedRoute, Router } from "@angular/router";
NgIf;

@Component({
  selector: "app-playlists-view",
  templateUrl: "./playlists-view.component.html",
  styleUrls: ["./playlists-view.component.scss"]
})
export class PlaylistsViewComponent implements OnInit {
  constructor(
    private router:Router,
    private route: ActivatedRoute) {

    this.route.paramMap.subscribe(paramMap => {
      const playlist_id = paramMap.get("playlist_id");
      
      const found = this.playlists.find(p => p.id === parseInt(playlist_id));
      if (found) {
        this.selected = found
      }
    });
  }

  mode = "show"; // 'edit'

  playlists: Playlist[] = [
    {
      id: 123,
      name: "Angular Hits",
      favorite: true,
      color: "#ff00ff"
    },
    {
      id: 234,
      name: "Angular Top20",
      favorite: false,
      color: "#ffff00"
    },
    {
      id: 345,
      name: "Best of Angular",
      favorite: true,
      color: "#00ffff"
    }
  ];

  selected: Playlist = this.playlists[0];

  select(playlist: Playlist) {
    this.router.navigate(['/playlists', playlist.id])
  }

  ngOnInit() {}

  onEdit() {
    this.mode = "edit";
  }

  onCancel() {
    this.mode = "show";
  }

  updatePlaylist(draft: Playlist) {
    const index = this.playlists.findIndex(p => p.id === draft.id);
    if (index !== -1) {
      this.playlists.splice(index, 1, draft);
    }
    this.select(draft);
    this.mode = "show";
  }
}
