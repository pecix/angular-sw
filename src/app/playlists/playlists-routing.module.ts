import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlaylistsViewComponent } from './views/playlists-view/playlists-view.component';


const routes: Routes = [
  {
    path: "playlists",
    component: PlaylistsViewComponent
  },
  {
    path: "playlists/:playlist_id",
    component: PlaylistsViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaylistsRoutingModule { }
