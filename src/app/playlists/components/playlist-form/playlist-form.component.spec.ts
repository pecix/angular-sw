import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { PlaylistFormComponent } from "./playlist-form.component";
import { By } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";

fdescribe("PlaylistFormComponent", () => {
  let component: PlaylistFormComponent;
  let fixture: ComponentFixture<PlaylistFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlaylistFormComponent],
      imports: [FormsModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistFormComponent);
    component = fixture.componentInstance;
    component.playlist = {
      id: 123,
      name: "Angular Hits",
      favorite: true,
      color: "#ff00ff"
    };
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should show data in form", () => {

    return fixture.whenStable().then(()=>{
      expect(findByCSS("input[name=name]").nativeElement.value).toEqual(
      "Angular Hits"
      );

    })
  });

  function findByCSS(css) {
    return fixture.debugElement.query(By.css(css));
  }
});
