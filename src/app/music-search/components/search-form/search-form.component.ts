import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  ChangeDetectionStrategy
} from "@angular/core";
import {
  FormGroup,
  FormControl,
  Validators,
  ValidatorFn,
  AbstractControl,
  ValidationErrors,
  AsyncValidatorFn
} from "@angular/forms";
import {
  filter,
  distinctUntilChanged,
  debounceTime,
  mapTo,
  map,
  withLatestFrom
} from "rxjs/operators";
import { Observable, PartialObserver, combineLatest } from "rxjs";

@Component({
  selector: "app-search-form",
  templateUrl: "./search-form.component.html",
  styleUrls: ["./search-form.component.scss"]
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchFormComponent implements OnInit {
  censor: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    const hasError =
      "string" == typeof control.value && control.value.includes("batman");

    return hasError
      ? {
          censor: { badword: "batman" }
        }
      : null;
  };

  asyncCensor: AsyncValidatorFn = (
    control: AbstractControl
  ): Observable<ValidationErrors | null> => {
    // return this.http.get('valduation',{}).pipe(map(resp=>errors|null))

    return Observable.create(
      (observer: PartialObserver<ValidationErrors | null>) => {
        const errors = this.censor(control);

        const handler = setTimeout(() => {
          observer.next(errors);
          observer.complete();
        }, 1000);

        /* onunsubsribe */
        return () => {
          clearTimeout(handler);
        };
      }
    );
  };

  queryForm = new FormGroup({
    query: new FormControl(
      "",
      [Validators.required, Validators.minLength(3)],
      [this.asyncCensor]
    )
  });

  @Input()
  set query(q) {
    this.queryForm.get("query").setValue(q, {
      emitEvent: false
    });
  }

  // ngOnChanges(){}

  constructor() {
    (window as any).form = this.queryForm;

    const field = this.queryForm.get("query");

    const values$ = field.valueChanges.pipe(
      debounceTime(400),
      filter(query => query.length >= 3),
      distinctUntilChanged()
    );

    const valid$ = field.statusChanges.pipe(
      filter(status => status === "VALID"),
      mapTo(true)
    );

    // const validvalues$ = combineLatest(valid$, values$).pipe(
    //    map(([tegoNiePotrzebujemy, value]) => value)
    // );

    const validvalues$ = valid$.pipe(
      withLatestFrom(values$),
      map(([tegoNiePotrzebujemy, value]) => value)
    );

    validvalues$.subscribe(query => {
      this.search(query);
    });
  }

  @Output() queryChange = new EventEmitter<string>();

  ngOnInit() {}

  search(query) {
    this.queryChange.emit(query);
  }
}
