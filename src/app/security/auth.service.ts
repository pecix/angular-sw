// https://github.com/manfredsteyer/angular-oauth2-oidc
import { Injectable } from "@angular/core";
import { HttpParams } from "@angular/common/http";

export class AuthConfig {
  auth_url: string;
  client_id: string;
  redirect_uri: string;
  response_type = "token";
}

@Injectable({
  providedIn: "root"
})
export class AuthService {
  token: string | null = null;

  constructor(private config: AuthConfig) {
    // Get from storage
    this.token = sessionStorage.getItem("token");

    if (!this.token) {
      // Get from params
      const params = new HttpParams({
        fromString: location.hash
      });
      const token = params.get("#access_token");

      // Save to storage
      if (token) {
        location.hash = "";
        this.token = token;
        sessionStorage.setItem("token", this.token);
      }
    }
  }

  authorize() {
    sessionStorage.removeItem('token')
    const { auth_url, response_type, client_id, redirect_uri } = this.config;

    const params = new HttpParams({
      fromObject: {
        response_type,
        client_id,
        redirect_uri
      }
    });
    const url = `${auth_url}?${params.toString()}`;

    location.href = url;
  }

  getToken() {
    if (!this.token) {
      this.authorize();
    }
    return this.token;
  }
}
